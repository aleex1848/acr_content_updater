﻿param(
[string]$TOKEN
);
if($TOKEN.Length -eq 0)
{
echo "No Token, no service!";
echo "Trag deinen Token in der Batch Datei ein Junge!";
}
$base_url="https://toolv3.aleex.de/api/";
$car_exists="content/exists/car/";
$track_exists="content/exists/track/";
$upload="content/upload";
$webcontent_dir = $PSScriptRoot+"\WEB_CONTENT";
$webcontent_file = $PSScriptRoot+"\WEB_CONTENT.zip";

$my_trackdirs =  Get-ChildItem $PSScriptRoot\tracks | ?{$_.PSISContainer};
$my_cardirs =  Get-ChildItem $PSScriptRoot\cars | ?{$_.PSISContainer};

$sz_path = $PSScriptRoot+"\7z.exe";
set-alias sz "$sz_path";
$target = $PSScriptRoot+"\WEB_CONTENT.zip";
$source = $webcontent_dir+"\*";

$found = $false;

echo "API TOKEN lautet $TOKEN";
if(Test-Path -Path $webcontent_dir ){
    echo "Lösche altes WEB_CONTENT Verzeichnis";
    Remove-Item -Recurse -Force $webcontent_dir;
}


if(Test-Path -Path $webcontent_file){
    echo "Lösche alte WEB_CONTENT.zip";
    Remove-Item -Recurse -Force $webcontent_file;
}





echo 'Prüfe Strecken';
foreach ($trackdir in $my_trackdirs) {    
    $body = @{
     "name"=$trackdir.Name;
    } | ConvertTo-Json

    $header = @{
     "Accept"="application/json"
     "Authorization"="Bearer $TOKEN"
     "Content-Type"="application/json"
    }     
    $response = Invoke-RestMethod -Uri $base_url$track_exists -Method 'Post' -Body $body -Headers $header;
    if ($response.exists -eq $false)
    {

xcopy /s /y $PSScriptRoot\tracks\$trackdir\*ui_track.json $webcontent_dir\tracks\$trackdir\ > $null
xcopy /s /y $PSScriptRoot\tracks\$trackdir\*preview.png $webcontent_dir\tracks\$trackdir\ > $null
xcopy /s /y $PSScriptRoot\tracks\$trackdir\*map.png $webcontent_dir\tracks\$trackdir\ > $null

        echo $response.msg;
        $found = $true;
    }
    

}

echo 'Prüfe Fahrzeuge';
foreach ($cardir in $my_cardirs) {

    $body = @{
     "name"=$cardir.Name;
    } | ConvertTo-Json

    $header = @{
     "Accept"="application/json"
     "Authorization"="Bearer $TOKEN"
     "Content-Type"="application/json"
    }     
    $response = Invoke-RestMethod -Uri $base_url$car_exists -Method 'Post' -Body $body -Headers $header;
    if ($response.exists -eq $false)
    {
        xcopy /s /y $PSScriptRoot\cars\$cardir\*ui_car.json $webcontent_dir\cars\$cardir\ > $null
        xcopy /s /y $PSScriptRoot\cars\$cardir\*preview.jpg $webcontent_dir\cars\$cardir\ > $null
        xcopy /s /y $PSScriptRoot\cars\$cardir\*badge.* $webcontent_dir\cars\$cardir\ > $null

        echo $response.msg;
        $found = $true;
    }
}

if ($found -eq $true)
{
    $header = @{
     "Authorization"="Bearer $TOKEN"
    }

    echo "Mache eine Zip Datei aus dem ganzen Kack";
    #Compress-Archive -Path $webcontent_dir\* -DestinationPath $webcontent_dir;
    #$PSScriptRoot+"\7z.exe a "+$PSScriptRoot+"\WEB_CONTENT.zip $webcontent_dir\*" 
    sz a $target $source > $null;


    $FilePath = $webcontent_dir+".zip";
    $fileBytes = [System.IO.File]::ReadAllBytes($FilePath);
    $fileEnc = [System.Text.Encoding]::GetEncoding('ISO-8859-1').GetString($fileBytes);
    $boundary = [System.Guid]::NewGuid().ToString(); 
    $LF = "`r`n";

    $bodyLines = ( 
        "--$boundary",
        "content-transfer-encoding: base64",
        "Content-Disposition: form-data; content-transfer-encoding: `"base64`"; name=""file""; filename=""temp.zip""",
        "Content-Type: application/zip$LF",
        $fileEnc,
        "--$boundary--$LF" 
    ) -join $LF


    echo "Und jetzt noch hochladen... DAUERT!";
    $response = Invoke-RestMethod -Uri $base_url$upload -Method Post -ContentType "multipart/form-data; boundary=""$boundary""" -Body $bodyLines -Headers $header -Verbose


    echo $response.msg;
}
else
{
    echo "Nichts neues gefunden, leg dich wieder hin.";
}
echo "FERTIG !";
